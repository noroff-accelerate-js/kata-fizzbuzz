for (let index = 0; index <= 100; index++) {
    console.log(fizzbuzz(index))
}

function fizzbuzz(number) {
    
    let string = "";

    if (number % 3 === 0)
        string += "Fizz"

    if (number % 5 === 0)
        string += "Buzz"

    return string === "" ? number : string
}